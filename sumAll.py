#prompt the user to enter numbers one line at a time
#last line must end with a zero
#keep a running some of the numbers (store in array and sum at zero?)
#only print sum after all numbers are entered
#DONT CREATE A LIST

#each time a number is read, immediately add it to sum, and be done w said number

def sumAll():

    print('Please enter numbers you would like to sum up. When you are finished, enter 0 to end. ')

    sum = 0
    boolean = True

    while(boolean):
        userInput = int(input('\n enter your numbers: '))
        sum = sum + userInput
        if(userInput == 0):
            boolean = False
    print('The sum of the numbers you have entered is: ', sum)

sumAll()
