#obtain age and length of citizenship from user
#age must be >=30
#Must be a US citizen >=9
#return that they're eligable to be eligible for the House and Senate

#if age is >=25
#and is a us citizen for >= 7
#return that they're only eligable to be a US representative

#else condition telling them they are ineligiable for Congress

#set up the program so that citizenship is first
#use if/else statement for other requirements

def requirements():
    age = int(input('Please enter your age: '))
    legal = int(input('Please enter how many years you have been a U.S. citizen for: '))

    if legal >= 9 and age >= 30:
        print('You are eligable for both the House and the Senate.')
    elif legal >= 7 and age >= 25:
        print('You are eligable only for the House.')
    else:
        print('You are ineligible for Congress.')



requirements()
    
